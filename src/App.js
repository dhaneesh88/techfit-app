import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './containers/Home/Home';
import Posts from './containers/Posts/Posts';
import Feedback from './containers/Feedback/Feedback';

class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route path="/posts" component={Posts} />
          <Route path="/feedback" component={Feedback} />
          <Route path="/" component={Home} />
        </Switch>
      </div>
    );
  }
}

export default App;
